package com.futurekits.managers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.futurekits.kit.AbstractKit;
import com.futurekits.kit.kits.Archer;
import com.futurekits.kit.kits.PvP;
import com.futurekits.plugin.FutureKits;

public class KitManager {

	private static Map<String, AbstractKit> kits = new HashMap<String, AbstractKit>();
	private static List<AbstractKit> kitList = new ArrayList<AbstractKit>();
	private static Map<UUID, AbstractKit> selected = new HashMap<UUID, AbstractKit>();

	/* Storage for the Kit instances */
	public static boolean isKit(String kitName) {
		return kits.containsKey(kitName.toLowerCase());
	}

	public static AbstractKit getKit(String kitName) {
		return kits.get(kitName.toLowerCase());
	}

	public static Collection<AbstractKit> getKits() {
		return kitList;
	}

	/* Kit selection for players */
	public static void selectKit(Player p, AbstractKit kit) {
		selectKit(p.getUniqueId(), kit);
	}

	public static void selectKit(UUID uuid, AbstractKit kit) {
		selected.put(uuid, kit);
	}

	public static void unSelectKit(Player p) {
		unSelectKit(p.getUniqueId());
	}

	public static void unSelectKit(UUID uuid) {
		selected.remove(uuid);
	}

	public static boolean hasKitSelected(Player p) {
		return hasKitSelected(p.getUniqueId());
	}

	public static AbstractKit getKitSelected(Player p) {
		return getKitSelected(p.getUniqueId());
	}

	public static AbstractKit getKitSelected(UUID uuid) {
		return selected.get(uuid);
	}

	public static boolean hasKitSelected(UUID uuid) {
		return selected.containsKey(uuid);
	}

	/* End */

	private final FutureKits plugin;

	public KitManager(FutureKits plugin) {
		this.plugin = plugin;
		loadKits();
	}

	private void loadKits() {
		handleKitInstance(new PvP());
		handleKitInstance(new Archer());
	}

	private void handleKitInstance(AbstractKit kit) {
		kits.put(kit.getName().toLowerCase(), kit);
		kitList.add(kit);
		plugin.getServer().getPluginManager().registerEvents(kit, plugin);
	}

}
