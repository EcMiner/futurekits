package com.futurekits.managers;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;

import com.futurekits.guis.kitgui.KitGUI;
import com.futurekits.plugin.FutureKits;
import com.futurekits.stats.Stats;
import com.futurekits.stats.StatsManager;
import com.futurekits.utilities.ItemUtil;

public class PlayerManager {

	private static FutureKits plugin;

	/* Items */
	public static final ItemStack kitSelector = ItemUtil.setItemMeta(new ItemStack(Material.CHEST), ChatColor.GREEN + "Kit Selector", ChatColor.GRAY + "Click me to choose a kit!");
	public static final ItemStack yourStats = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_NUGGET), ChatColor.GREEN + "Your Stats", ChatColor.GRAY + "Click me to see your stats!");
	public static final ItemStack eventMenu = ItemUtil.setItemMeta(new ItemStack(Material.PAPER), ChatColor.GREEN + "Event Menu", ChatColor.GRAY + "Click me to open the Event Menu!");
	public static final ItemStack teleport1v1 = ItemUtil.setItemMeta(new ItemStack(Material.WOOD_SWORD), ChatColor.GREEN + "Teleport to the 1v1", ChatColor.GRAY + "Click me to teleport to the 1v1!");
	public static final ItemStack kitShop = ItemUtil.setItemMeta(new ItemStack(Material.EMERALD), ChatColor.GREEN + "Kit Shop", ChatColor.GRAY + "Click me to buy kits with your credits!");

	public static final ItemStack yourKills = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND_SWORD), ChatColor.GOLD + "Your Kills:");
	public static final ItemStack yourDeaths = ItemUtil.setItemMeta(new ItemStack(Material.REDSTONE), ChatColor.GOLD + "Your Deaths:");
	public static final ItemStack yourKdr = ItemUtil.setItemMeta(new ItemStack(Material.IRON_INGOT), ChatColor.GOLD + "Your Kdr:");
	public static final ItemStack yourCoins = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_INGOT), ChatColor.GOLD + "Your Coins:");
	public static final ItemStack yourBestKs = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND), ChatColor.GOLD + "Your best Killstreak:");
	public static final ItemStack yourTeam = ItemUtil.setItemMeta(new ItemStack(Material.MUSHROOM_SOUP), ChatColor.GOLD + "Your Team:");

	public PlayerManager(FutureKits instance) {
		plugin = instance;
	}

	public static void equipSpawnItems(Player p) {
		clearPlayer(p);

		PlayerInventory inv = p.getInventory();

		inv.setItem(0, kitSelector);
		inv.setItem(2, yourStats);
		inv.setItem(4, eventMenu);
		inv.setItem(6, teleport1v1);
		inv.setItem(8, kitShop);
	}

	public static void openStatsInventory(Player p) {
		Inventory inv = Bukkit.createInventory(p, 9, ChatColor.GREEN + "Your Stats");
		Stats stats = StatsManager.getStats(p);
		inv.setItem(0, ItemUtil.setLore(yourKills.clone(), ChatColor.DARK_AQUA.toString() + stats.getKills()));
		inv.setItem(1, ItemUtil.setLore(yourDeaths.clone(), ChatColor.DARK_AQUA.toString() + stats.getDeaths()));
		inv.setItem(2, ItemUtil.setLore(yourKdr.clone(), ChatColor.DARK_AQUA.toString() + (Double.valueOf(Math.round(stats.getKDRatio() * 100)) / 100d)));
		inv.setItem(3, ItemUtil.setLore(yourCoins.clone(), ChatColor.DARK_AQUA.toString() + stats.getCoins()));
		inv.setItem(4, ItemUtil.setLore(yourBestKs.clone(), ChatColor.DARK_AQUA.toString() + stats.getBestKillStreak()));
		inv.setItem(5, ItemUtil.setLore(yourTeam.clone(), stats.isInTeam() ? ChatColor.DARK_AQUA + stats.getTeamName() : ChatColor.RED + "NaN"));
		inv.setItem(6, KitGUI.nothing);
		inv.setItem(7, KitGUI.nothing);
		inv.setItem(8, KitGUI.nothing);
		p.openInventory(inv);
	}

	public static void teleportToSpawn(Player p) {

	}

	public static void forceRespawn(final Player p) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

			@Override
			public void run() {
				try {
					Object nmsPlayer = p.getClass().getMethod("getHandle").invoke(p);
					Object packet = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".PacketPlayInClientCommand").newInstance();
					Class<?> enumClass = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".EnumClientCommand");

					for (Object ob : enumClass.getEnumConstants()) {
						if (ob.toString().equals("PERFORM_RESPAWN")) {
							packet = packet.getClass().getConstructor(enumClass).newInstance(ob);
						}
					}

					Object con = nmsPlayer.getClass().getField("playerConnection").get(nmsPlayer);
					con.getClass().getMethod("a", packet.getClass()).invoke(con, packet);
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}

		}, 1);
	}

	public static void clearPlayer(Player p) {
		p.getInventory().clear();
		p.getInventory().setArmorContents(null);
		for (PotionEffect pe : p.getActivePotionEffects()) {
			p.removePotionEffect(pe.getType());
		}

		if (p.getOpenInventory().getTopInventory() != null) {
			if (p.getOpenInventory().getTopInventory().getType() == InventoryType.CRAFTING) {
				p.getOpenInventory().getTopInventory().clear();
			}
		}
	}

}
