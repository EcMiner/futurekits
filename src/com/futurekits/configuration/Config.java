package com.futurekits.configuration;

import java.io.File;
import java.io.IOException;

import net.minecraft.util.org.apache.commons.io.FileUtils;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {

	private FileConfiguration config;
	private File file;

	public Config(File file) {
		this.file = file;
		this.config = YamlConfiguration.loadConfiguration(file);
	}

	public FileConfiguration getConfig() {
		return config;
	}

	public File getFile() {
		return file;
	}

	public File getParentFolder() {
		return file.getParentFile();
	}

	public final boolean saveDefaultConfig() {
		if (!file.exists()) {
			File f = new File(file.getName());
			if (f.exists()) {
				try {
					FileUtils.copyFile(f, file);
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
				return true;
			} else {
				throw new NullPointerException("The resource '" + file.getName() + "' does not exist in the plugin file!");
			}
		}
		return false;
	}

	public final void saveConfig() {
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
