package com.futurekits.sqlite;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;

import com.futurekits.plugin.FutureKits;

public class SQLite {

	private static final List<SQLite> instances = new ArrayList<SQLite>();

	public static void closeAll() {
		for (SQLite db : instances) {
			try {
				db.c.close();
			} catch (SQLException e) {
			}
		}
	}

	private Connection c = null;
	private FutureKits plugin;

	public SQLite(FutureKits plugin, File file) {
		if (!file.exists()) {
			try {
				file.createNewFile();
				System.out.println("Creating new file!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.plugin = plugin;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
		instances.add(this);
	}

	public void execute(final String query) {
		try {
			c.createStatement().execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateQuery(final String query) {
		new BukkitRunnable() {

			@Override
			public void run() {
				try {
					c.createStatement().executeUpdate(query);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}.runTaskAsynchronously(plugin);
	}

	public ResultSet executeQuery(final String query) {
		try {
			return c.createStatement().executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void close() {
		try {
			c.close();
			instances.remove(this);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
