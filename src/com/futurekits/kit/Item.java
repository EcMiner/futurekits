package com.futurekits.kit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.futurekits.utilities.ItemUtil;

public class Item {

	private ItemStack item;
	private int slot;

	public Item(ItemStack item, int slot, String displayName, String lore) {
		this(item, displayName, slot, ItemUtil.formatLore(ChatColor.GRAY, lore));
	}

	public Item(ItemStack item, int slot, String displayName, String lore, ChatColor beginColor) {
		this(item, displayName, slot, ItemUtil.formatLore(beginColor, lore));
	}

	public Item(ItemStack item, String displayName, int slot, List<String> lore) {
		this.item = ItemUtil.setItemMeta(item, displayName, lore);
		this.slot = slot;
	}

	public Item(ItemStack item, int slot, String displayName, String... lore) {
		this(item, displayName, slot, Arrays.asList(lore));
	}

	public Item(ItemStack item, int slot, String displayName) {
		this(item, displayName, slot, new ArrayList<String>());
	}

	public Item(ItemStack item, int slot) {
		this(item, null, slot, new ArrayList<String>());
	}

	public ItemStack getItem() {
		return item;
	}

	public int getSlot() {
		return slot;
	}

	public boolean isItem(ItemStack item) {
		return item.hasItemMeta() && item.getItemMeta().getDisplayName() == this.item.getItemMeta().getDisplayName() && item.getItemMeta().getLore() == this.item.getItemMeta().getLore();
	}

	public void addEnchantment(Enchantment ench, int lvl) {
		this.item.addUnsafeEnchantment(ench, lvl);
	}

}
