package com.futurekits.kit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.Permission;

import com.futurekits.utilities.ItemUtil;

public abstract class AbstractKit implements Listener {

	private String name;
	private ItemStack icon;
	private String description;
	private List<String> formattedDescription;
	private Permission permission;
	private int price;

	private Map<Integer, Item> items = new HashMap<Integer, Item>();

	private ItemStack helmet;
	private ItemStack chestplate;
	private ItemStack leggings;
	private ItemStack boots;

	private ItemStack sword = new ItemStack(Material.STONE);

	private int redMushrooms = 32;
	private int brownMushrooms = 32;
	private int bowls = 32;

	public AbstractKit(String name, ItemStack icon, String description, int price) {
		this.name = name;
		this.description = description;
		this.formattedDescription = ItemUtil.formatLore(ChatColor.GOLD, description);
		this.icon = ItemUtil.setItemMeta(icon, name, formattedDescription);
		this.permission = new Permission("futurekits.kit." + name.toLowerCase());
		this.price = price;
	}

	public final String getName() {
		return name;
	}

	public final ItemStack getIcon() {
		return icon;
	}

	public final String getDescription() {
		return description;
	}

	public final List<String> getFormattedDescription() {
		return formattedDescription;
	}

	public Permission getPermission() {
		return permission;
	}

	public int getPrice() {
		return price;
	}

	public final void addItem(Item item) {
		items.put(item.getSlot(), item);
	}

	public final boolean isItem(ItemStack item, int slot) {
		return items.containsKey(slot) ? items.get(slot).isItem(item) : false;
	}

	/* Setting armor and sword */
	public final void setHelmet(Material helmet) {
		this.helmet.setType(helmet);
	}

	public final void setChestplate(Material chestplate) {
		this.chestplate.setType(chestplate);
	}

	public final void setLeggings(Material leggings) {
		this.leggings.setType(leggings);
	}

	public final void setBoots(Material boots) {
		this.boots.setType(boots);
	}

	public final void setArmourSet(ArmourSet set) {
		setHelmet(set.getHelmet());
		setChestplate(set.getChestplate());
		setLeggings(set.getLeggings());
		setBoots(set.getBoots());
	}

	public final void setSword(Material sword) {
		this.sword.setType(sword);
	}

	/* End of setting armor and sword */

	public final ItemStack getHelmet() {
		return helmet;
	}

	public final ItemStack getChestplate() {
		return chestplate;
	}

	public final ItemStack getLeggings() {
		return leggings;
	}

	public final ItemStack getBoots() {
		return boots;
	}

	public final Map<Integer, Item> getItems() {
		return items;
	}

	public final void setRedMushroomsAmoun(int amount) {
		this.redMushrooms = amount;
	}

	public final void setBrownMushroomsAmount(int amount) {
		this.brownMushrooms = amount;
	}

	public final void setBowlsAmount(int amount) {
		this.bowls = amount;
	}

	public final void setSoupAmount(int amount) {
		setRedMushroomsAmoun(amount);
		setBrownMushroomsAmount(amount);
		setBowlsAmount(amount);
	}

	/* Enchantment */
	private void enchant(ItemStack item, Enchantment enchantment, int lvl) {
		if (item != null && enchantment != null)
			item.addUnsafeEnchantment(enchantment, lvl);
	}

	public void addSwordEnchantment(Enchantment enchantment, int lvl) {
		enchant(sword, enchantment, lvl);
	}

	public void addHelmetEnchantment(Enchantment enchantment, int lvl) {
		enchant(helmet, enchantment, lvl);
	}

	public void addChestplateEnchantment(Enchantment enchantment, int lvl) {
		enchant(chestplate, enchantment, lvl);
	}

	public void addLeggingsEnchantment(Enchantment enchantment, int lvl) {
		enchant(leggings, enchantment, lvl);
	}

	public void addBootsEnchantment(Enchantment enchantment, int lvl) {
		enchant(boots, enchantment, lvl);
	}

	public void addArmourEnchantment(Enchantment enchantment, int lvl) {
		addHelmetEnchantment(enchantment, lvl);
		addChestplateEnchantment(enchantment, lvl);
		addLeggingsEnchantment(enchantment, lvl);
		addBootsEnchantment(enchantment, lvl);
	}

	public void addItemEnchantment(int item, Enchantment enchantment, int lvl) {
		if (items.containsKey(item)) {
			items.get(item).addEnchantment(enchantment, lvl);
		}
	}

	/* End of enchantment */

	public final int getBowlsAmount() {
		return bowls;
	}

	public final int getRedMushroomsAmount() {
		return redMushrooms;
	}

	public final int getBrownMushroomsAmount() {
		return brownMushrooms;
	}

	public final void equip(Player p) {
		PlayerInventory inv = p.getInventory();
		inv.clear();
		inv.setArmorContents(null);

		inv.setItem(0, sword);
		inv.setItem(14, new ItemStack(Material.BOWL, bowls));
		inv.setItem(15, new ItemStack(Material.BROWN_MUSHROOM, brownMushrooms));
		inv.setItem(16, new ItemStack(Material.RED_MUSHROOM, redMushrooms));
		inv.setArmorContents(new ItemStack[] { boots, leggings, chestplate, helmet });

		for (Map.Entry<Integer, Item> entry : items.entrySet()) {
			inv.setItem(entry.getKey(), entry.getValue().getItem());
		}

		for (int i = 0; i < inv.getSize(); i++) {
			inv.addItem(new ItemStack(Material.MUSHROOM_SOUP));
		}
	}

}
