package com.futurekits.kit.kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.futurekits.kit.AbstractKit;
import com.futurekits.kit.Item;

public class Archer extends AbstractKit {

	public Archer() {
		super("Archer", new ItemStack(Material.BOW), "Get 10 arrows and a power 1 bow that does massive damage!", 25000);
		addItem(new Item(new ItemStack(Material.BOW), 8, ChatColor.GREEN + "Bow"));
		addItemEnchantment(8, Enchantment.ARROW_DAMAGE, 1);

		addItem(new Item(new ItemStack(Material.ARROW, 10), 9));
	}

}
