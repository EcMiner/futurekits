package com.futurekits.kit.kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.futurekits.kit.AbstractKit;

public class PvP extends AbstractKit {

	public PvP() {
		super("PvP", new ItemStack(Material.DIAMOND_SWORD), "Default PvP kit!", 25000);
		setSword(Material.WOOD_SWORD);
		addSwordEnchantment(Enchantment.DURABILITY, 10);
	}

}
