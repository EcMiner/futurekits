package com.futurekits.kit;

import org.bukkit.Material;

public enum ArmourSet {

	FULL_LEATHER("leather"), FULL_GOLD("gold"), FULL_IRON("iron"), FULL_DIAMOND("diamond");

	private Material helmet;
	private Material chestplate;
	private Material leggings;
	private Material boots;

	private ArmourSet(Material helmet, Material chestplate, Material leggings, Material boots) {
		this.helmet = helmet;
		this.chestplate = chestplate;
		this.leggings = leggings;
		this.boots = boots;
	}

	private ArmourSet(String material) {
		this(Material.getMaterial(material.toUpperCase() + "_HELMET"), Material.getMaterial(material.toUpperCase() + "_CHESTPLATE"), Material.getMaterial(material.toUpperCase() + "_LEGGINGS"), Material.getMaterial(material.toUpperCase() + "_BOOTs"));
	}

	public Material getHelmet() {
		return helmet;
	}

	public Material getChestplate() {
		return chestplate;
	}

	public Material getLeggings() {
		return leggings;
	}

	public Material getBoots() {
		return boots;
	}

}