package com.futurekits.onevone.listeners;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.futurekits.onevone.GUIItems;
import com.futurekits.onevone.PlayerData;
import com.futurekits.utilities.InventoryUtil;
import com.futurekits.utilities.ItemUtil;

public class OptionListener implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (PlayerData.hasPlayerData(p) && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) && e.getItem() != null) {
			PlayerData data = PlayerData.getPlayerData(p);
			ItemStack item = e.getItem();
			if (ItemUtil.isSameItem(item, GUIItems.edit_options)) {
				p.openInventory(data.getMainInventory());
			}
		}
	}

	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (PlayerData.hasPlayerData(p)) {
			PlayerData data = PlayerData.getPlayerData(p);
			ItemStack item = e.getItemDrop().getItemStack();
		}
	}

	private Map<UUID, Long> clickTimeout = new HashMap<UUID, Long>();

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if (PlayerData.hasPlayerData(p) && InventoryUtil.isValid(e.getInventory(), e.getRawSlot())) {
			PlayerData data = PlayerData.getPlayerData(p);
			if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Edit 1v1 options")) {
				e.setCancelled(true);
				switch (e.getRawSlot()) {
				case 13:
					p.openInventory(data.getHelmetInventory());
					break;
				case 21:
					p.openInventory(data.getSwordInventory());
					break;
				case 22:
					p.openInventory(data.getChestplateInventory());
					break;
				case 23:
					p.openInventory(data.getEffectsInventory());
					break;
				case 31:
					p.openInventory(data.getLeggingsInventory());
					break;
				case 40:
					p.openInventory(data.getBootsInventory());
					break;
				}
			} else if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Choose sword type")) {
				e.setCancelled(true);
				if (e.getRawSlot() == 0) {
					p.openInventory(data.getMainInventory());
				} else if (e.getRawSlot() >= 18 && e.getRawSlot() < 24) {
					data.setSword(e.getCurrentItem().getType());
					return;
				}
				if (canClick(p)) {
					if (e.getRawSlot() == 24) {
						data.toggleEnchantment("sword", Enchantment.DAMAGE_ALL, 3);
						data.updateSwordEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					} else if (e.getRawSlot() == 25) {
						data.toggleEnchantment("sword", Enchantment.KNOCKBACK, 3);
						data.updateSwordEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					} else if (e.getRawSlot() == 26) {
						data.toggleEnchantment("sword", Enchantment.FIRE_ASPECT, 3);
						data.updateSwordEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					}
				}
			} else if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Choose helmet type")) {
				e.setCancelled(true);
				if (e.getRawSlot() == 0) {
					p.openInventory(data.getMainInventory());
				} else if (e.getRawSlot() >= 18 && e.getRawSlot() < 25) {
					data.setHelmet(e.getCurrentItem().getType());
				}
				if (canClick(p)) {
					if (e.getRawSlot() == 25) {
						data.toggleEnchantment("helmet", Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						data.updateHelmetEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					} else if (e.getRawSlot() == 26) {
						data.toggleEnchantment("helmet", Enchantment.THORNS, 3);
						data.updateHelmetEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					}
				}
			} else if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Choose chestplate type")) {
				e.setCancelled(true);
				if (e.getRawSlot() == 0) {
					p.openInventory(data.getMainInventory());
				} else if (e.getRawSlot() >= 18 && e.getRawSlot() < 25) {
					data.setChestplate(e.getCurrentItem().getType());
				}
				if (canClick(p)) {
					if (e.getRawSlot() == 25) {
						data.toggleEnchantment("chestplate", Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						data.updateChestplateEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					} else if (e.getRawSlot() == 26) {
						data.toggleEnchantment("chestplate", Enchantment.THORNS, 3);
						data.updateChestplateEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					}
				}
			} else if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Choose leggings type")) {
				e.setCancelled(true);
				if (e.getRawSlot() == 0) {
					p.openInventory(data.getMainInventory());
				} else if (e.getRawSlot() >= 18 && e.getRawSlot() < 25) {
					data.setLeggings(e.getCurrentItem().getType());
				}
				if (canClick(p)) {
					if (e.getRawSlot() == 25) {
						data.toggleEnchantment("leggings", Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						data.updateLeggingsEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					} else if (e.getRawSlot() == 26) {
						data.toggleEnchantment("leggings", Enchantment.THORNS, 3);
						data.updateLeggingsEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					}
				}
			} else if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Choose boots type")) {
				e.setCancelled(true);
				if (e.getRawSlot() == 0) {
					p.openInventory(data.getMainInventory());
				} else if (e.getRawSlot() >= 18 && e.getRawSlot() < 25) {
					data.setBoots(e.getCurrentItem().getType());
				}
				if (canClick(p)) {
					if (e.getRawSlot() == 25) {
						data.toggleEnchantment("boots", Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						data.updateBootsEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					} else if (e.getRawSlot() == 26) {
						data.toggleEnchantment("boots", Enchantment.THORNS, 3);
						data.updateBootsEnchantments();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
					}
				}
			} else if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Edit potion effects")) {
				e.setCancelled(true);
				if (canClick(p)) {
					switch (e.getRawSlot()) {
					case 0:
						p.openInventory(data.getMainInventory());
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
						break;
					case 3:
						data.toggleSpeed();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
						break;
					case 4:
						data.toggleStrength();
						clickTimeout.put(p.getUniqueId(), System.currentTimeMillis());
						break;
					}
				}
			}
		}
	}

	private boolean canClick(Player p) {
		long l = clickTimeout.containsKey(p.getUniqueId()) ? clickTimeout.get(p.getUniqueId()) : 0;
		return (l - System.currentTimeMillis()) < -100;
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (e.getInventory().getTitle().equals(ChatColor.DARK_GREEN + "Edit potion effects")) {
			clickTimeout.remove(e.getPlayer().getUniqueId());
		}
	}

	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		Player p = e.getEntity();
		if (PlayerData.hasPlayerData(p)) {
			PlayerData data = PlayerData.getPlayerData(p);
			Iterator<ItemStack> drops = e.getDrops().iterator();
			while (drops.hasNext()) {
				ItemStack item = drops.next();
			}
		}
	}

}
