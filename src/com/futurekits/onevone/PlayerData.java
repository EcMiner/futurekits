package com.futurekits.onevone;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.futurekits.managers.PlayerManager;
import com.futurekits.utilities.ItemUtil;

public class PlayerData {

	private static Map<UUID, PlayerData> allData = new HashMap<UUID, PlayerData>();

	public static PlayerData getPlayerData(Player p) {
		return allData.get(p.getUniqueId());
	}

	public static boolean hasPlayerData(Player p) {
		return allData.containsKey(p.getUniqueId());
	}

	private Player p;

	/* Armour data */
	private ItemStack helmet = ItemUtil.setItemMeta(new ItemStack(Material.IRON_HELMET), ChatColor.GREEN + "Helmet: " + ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to change the helmet");
	private ItemStack chestplate = ItemUtil.setItemMeta(new ItemStack(Material.IRON_CHESTPLATE), ChatColor.GREEN + "Chestplate: " + ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to change the chestplate");
	private ItemStack leggings = ItemUtil.setItemMeta(new ItemStack(Material.IRON_LEGGINGS), ChatColor.GREEN + "Leggings: " + ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to change the leggings");
	private ItemStack boots = ItemUtil.setItemMeta(new ItemStack(Material.IRON_BOOTS), ChatColor.GREEN + "Boots: " + ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to change the boots");

	private ItemStack sword = ItemUtil.setItemMeta(new ItemStack(Material.IRON_SWORD), ChatColor.GREEN + "Sword: " + ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to change the sword");

	private Map<String, Map<Enchantment, Integer>> enchantments = new HashMap<String, Map<Enchantment, Integer>>();

	private int speedLvL = 0;
	private int strengthLvL = 0;

	/* Inventories */
	private Inventory mainInv = Bukkit.createInventory(null, 9 * 6, ChatColor.DARK_GREEN + "Edit 1v1 options");
	private Inventory swordInv = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GREEN + "Choose sword type");
	private Inventory helmetInv = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GREEN + "Choose helmet type");
	private Inventory chestplateInv = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GREEN + "Choose chestplate type");
	private Inventory leggingsInv = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GREEN + "Choose leggings type");
	private Inventory bootsInv = Bukkit.createInventory(null, 9 * 3, ChatColor.DARK_GREEN + "Choose boots type");
	private Inventory potionsInv = Bukkit.createInventory(null, 9, ChatColor.DARK_GREEN + "Edit potion effects");

	public PlayerData(Player p) {
		this.p = p;
		allData.put(p.getUniqueId(), this);

		updateMainInventory();

		updateSwordInventory();
		updateSwordEnchantments();

		updateHelmetInventory();
		updateHelmetEnchantments();

		updateChestplateInventory();
		updateChestplateEnchantments();

		updateLeggingsInventory();
		updateLeggingsEnchantments();

		updateBootsInventory();
		updateBootsEnchantments();

		updateEffectsInventory();
	}

	public void setHelmet(Material helmet) {
		this.helmet.setType(helmet);
		ItemUtil.setDisplayName(this.helmet, ChatColor.GREEN + "Helmet: " + ChatColor.AQUA + name(helmet));

		updateMainInventory();
		updateHelmetInventory();
	}

	public void setChestplate(Material chestplate) {
		this.chestplate.setType(chestplate);
		ItemUtil.setDisplayName(this.chestplate, ChatColor.GREEN + "Chestplate: " + ChatColor.AQUA + name(chestplate));

		updateMainInventory();
		updateChestplateInventory();
	}

	public void setLeggings(Material leggings) {
		this.leggings.setType(leggings);
		ItemUtil.setDisplayName(this.leggings, ChatColor.GREEN + "Leggings: " + ChatColor.AQUA + name(leggings));

		updateMainInventory();
		updateLeggingsInventory();
	}

	public void setBoots(Material boots) {
		this.boots.setType(boots);
		ItemUtil.setDisplayName(this.boots, ChatColor.GREEN + "Boots: " + ChatColor.AQUA + name(boots));

		updateMainInventory();
		updateBootsInventory();
	}

	public void setSword(Material sword) {
		this.sword.setType(sword);
		ItemUtil.setDisplayName(this.sword, ChatColor.GREEN + "Sword: " + ChatColor.AQUA + name(sword));

		updateMainInventory();
		updateSwordInventory();
	}

	public void toggleSpeed() {
		++speedLvL;
		if (speedLvL == 4) {
			speedLvL = 0;
		}
		updateEffectsInventory();
	}

	public void toggleStrength() {
		++strengthLvL;
		if (strengthLvL == 4) {
			strengthLvL = 0;
		}
		updateEffectsInventory();
	}

	public void toggleEnchantment(String itemName, Enchantment enchantment, int maxLevel) {
		int currentLvL = getEnchantment(itemName, enchantment);
		setEnchantment(itemName, enchantment, currentLvL + 1 <= maxLevel ? currentLvL + 1 : 0);
	}

	public ItemStack getHelmet() {
		return helmet;
	}

	public ItemStack getChestplate() {
		return chestplate;
	}

	public ItemStack getLeggings() {
		return leggings;
	}

	public ItemStack getBoots() {
		return boots;
	}

	public ItemStack getSword() {
		return sword;
	}

	public int getEnchantment(String forItem, Enchantment enchantment) {
		forItem = forItem.toLowerCase();
		if (enchantments.containsKey(forItem) && enchantments.get(forItem).containsKey(enchantment)) {
			return enchantments.get(forItem).get(enchantment);
		}
		return 0;
	}

	public void setEnchantment(String forItem, Enchantment enchantment, int lvl) {
		forItem = forItem.toLowerCase();
		if (lvl > 0) {
			if (!enchantments.containsKey(forItem)) {
				enchantments.put(forItem, new HashMap<Enchantment, Integer>());
			}
			enchantments.get(forItem).put(enchantment, lvl);
		} else {
			if (enchantments.containsKey(forItem)) {
				enchantments.get(forItem).remove(enchantment);
			}
		}
	}

	private String name(Material mat) {
		if (mat != Material.THIN_GLASS) {
			if (mat.name().startsWith("LEATHER_")) {
				return "Leather";
			} else if (mat.name().startsWith("GOLD_")) {
				return "Gold";
			} else if (mat.name().startsWith("IRON_")) {
				return "Iron";
			} else if (mat.name().startsWith("DIAMOND_")) {
				return "Diamond";
			} else {
				return "Not recognized";
			}
		} else {
			return "None";
		}
	}

	public void destroy() {
		allData.remove(p.getName());
		this.p = null;
	}

	/* GUIS */
	public Inventory getMainInventory() {
		return mainInv;
	}

	public Inventory getSwordInventory() {
		return swordInv;
	}

	public Inventory getHelmetInventory() {
		return helmetInv;
	}

	public Inventory getChestplateInventory() {
		return chestplateInv;
	}

	public Inventory getLeggingsInventory() {
		return leggingsInv;
	}

	public Inventory getBootsInventory() {
		return bootsInv;
	}

	public Inventory getEffectsInventory() {
		return potionsInv;
	}

	/* Inventory update */
	private void updateMainInventory() {
		mainInv.setItem(13, helmet);
		mainInv.setItem(21, sword);
		mainInv.setItem(22, chestplate);
		mainInv.setItem(23, GUIItems.potion_effects);
		mainInv.setItem(31, leggings);
		mainInv.setItem(40, boots);
	}

	private void updateSwordInventory() {
		swordInv.setItem(0, GUIItems.back);
		swordInv.setItem(4, sword);

		int slot = 18;

		if (sword.getType() != Material.WOOD_SWORD) {
			swordInv.setItem(slot, GUIItems.wood_sword);
			++slot;
		}
		if (sword.getType() != Material.STONE_SWORD) {
			swordInv.setItem(slot, GUIItems.stone_sword);
			++slot;
		}
		if (sword.getType() != Material.GOLD_SWORD) {
			swordInv.setItem(slot, GUIItems.gold_sword);
			++slot;
		}
		if (sword.getType() != Material.IRON_SWORD) {
			swordInv.setItem(slot, GUIItems.iron_sword);
			++slot;
		}
		if (sword.getType() != Material.DIAMOND_SWORD) {
			swordInv.setItem(slot, GUIItems.diamond_sword);
		}
	}

	public void updateSwordEnchantments() {
		swordInv.setItem(24, ItemUtil.setDisplayName(GUIItems.sword_sharpness.clone(), GUIItems.sword_sharpness.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("sword", Enchantment.DAMAGE_ALL) + "")));
		swordInv.setItem(25, ItemUtil.setDisplayName(GUIItems.sword_knockback.clone(), GUIItems.sword_knockback.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("sword", Enchantment.KNOCKBACK) + "")));
		swordInv.setItem(26, ItemUtil.setDisplayName(GUIItems.sword_fireaspect.clone(), GUIItems.sword_fireaspect.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("sword", Enchantment.FIRE_ASPECT) + "")));
	}

	private void updateHelmetInventory() {
		helmetInv.setItem(0, GUIItems.back);
		helmetInv.setItem(4, helmet);

		int slot = 18;

		if (helmet.getType() != Material.LEATHER_HELMET) {
			helmetInv.setItem(slot, GUIItems.leather_helmet);
			++slot;
		}
		if (helmet.getType() != Material.CHAINMAIL_HELMET) {
			helmetInv.setItem(slot, GUIItems.chain_helmet);
			++slot;
		}
		if (helmet.getType() != Material.GOLD_HELMET) {
			helmetInv.setItem(slot, GUIItems.gold_helmet);
			++slot;
		}
		if (helmet.getType() != Material.IRON_HELMET) {
			helmetInv.setItem(slot, GUIItems.iron_helmet);
			++slot;
		}
		if (helmet.getType() != Material.DIAMOND_HELMET) {
			helmetInv.setItem(slot, GUIItems.diamond_helmet);
			++slot;
		}
		if (helmet.getType() != Material.THIN_GLASS) {
			helmetInv.setItem(slot, GUIItems.none);
		}
	}

	public void updateHelmetEnchantments() {
		helmetInv.setItem(25, ItemUtil.setDisplayName(GUIItems.armour_protection.clone(), GUIItems.armour_protection.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("helmet", Enchantment.PROTECTION_ENVIRONMENTAL) + "")));
		helmetInv.setItem(26, ItemUtil.setDisplayName(GUIItems.armour_thorns.clone(), GUIItems.armour_thorns.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("helmet", Enchantment.THORNS) + "")));
	}

	private void updateChestplateInventory() {
		chestplateInv.setItem(0, GUIItems.back);
		chestplateInv.setItem(4, chestplate);

		int slot = 18;

		if (chestplate.getType() != Material.LEATHER_CHESTPLATE) {
			chestplateInv.setItem(slot, GUIItems.leather_chestplate);
			++slot;
		}
		if (chestplate.getType() != Material.CHAINMAIL_CHESTPLATE) {
			chestplateInv.setItem(slot, GUIItems.chain_chestplate);
			++slot;
		}
		if (chestplate.getType() != Material.GOLD_CHESTPLATE) {
			chestplateInv.setItem(slot, GUIItems.gold_chestplate);
			++slot;
		}
		if (chestplate.getType() != Material.IRON_CHESTPLATE) {
			chestplateInv.setItem(slot, GUIItems.iron_chestplate);
			++slot;
		}
		if (chestplate.getType() != Material.DIAMOND_CHESTPLATE) {
			chestplateInv.setItem(slot, GUIItems.diamond_chestplate);
			++slot;
		}
		if (chestplate.getType() != Material.THIN_GLASS) {
			chestplateInv.setItem(slot, GUIItems.none);
		}
	}

	public void updateChestplateEnchantments() {
		chestplateInv.setItem(25, ItemUtil.setDisplayName(GUIItems.armour_protection.clone(), GUIItems.armour_protection.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("chestplate", Enchantment.PROTECTION_ENVIRONMENTAL) + "")));
		chestplateInv.setItem(26, ItemUtil.setDisplayName(GUIItems.armour_thorns.clone(), GUIItems.armour_thorns.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("chestplate", Enchantment.THORNS) + "")));
	}

	private void updateLeggingsInventory() {
		leggingsInv.setItem(0, GUIItems.back);
		leggingsInv.setItem(4, leggings);

		int slot = 18;

		if (leggings.getType() != Material.LEATHER_LEGGINGS) {
			leggingsInv.setItem(slot, GUIItems.leather_leggings);
			++slot;
		}
		if (leggings.getType() != Material.CHAINMAIL_LEGGINGS) {
			leggingsInv.setItem(slot, GUIItems.chain_leggings);
			++slot;
		}
		if (leggings.getType() != Material.GOLD_LEGGINGS) {
			leggingsInv.setItem(slot, GUIItems.gold_leggings);
			++slot;
		}
		if (leggings.getType() != Material.IRON_LEGGINGS) {
			leggingsInv.setItem(slot, GUIItems.iron_leggings);
			++slot;
		}
		if (leggings.getType() != Material.DIAMOND_LEGGINGS) {
			leggingsInv.setItem(slot, GUIItems.diamond_leggings);
			++slot;
		}
		if (leggings.getType() != Material.THIN_GLASS) {
			leggingsInv.setItem(slot, GUIItems.none);
		}
	}

	public void updateLeggingsEnchantments() {
		leggingsInv.setItem(25, ItemUtil.setDisplayName(GUIItems.armour_protection.clone(), GUIItems.armour_protection.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("leggings", Enchantment.PROTECTION_ENVIRONMENTAL) + "")));
		leggingsInv.setItem(26, ItemUtil.setDisplayName(GUIItems.armour_thorns.clone(), GUIItems.armour_thorns.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("leggings", Enchantment.THORNS) + "")));
	}

	private void updateBootsInventory() {
		bootsInv.setItem(0, GUIItems.back);
		bootsInv.setItem(4, boots);

		int slot = 18;

		if (boots.getType() != Material.LEATHER_BOOTS) {
			bootsInv.setItem(slot, GUIItems.leather_boots);
			++slot;
		}
		if (boots.getType() != Material.CHAINMAIL_BOOTS) {
			bootsInv.setItem(slot, GUIItems.chain_boots);
			++slot;
		}
		if (boots.getType() != Material.GOLD_BOOTS) {
			bootsInv.setItem(slot, GUIItems.gold_boots);
			++slot;
		}
		if (boots.getType() != Material.IRON_BOOTS) {
			bootsInv.setItem(slot, GUIItems.iron_boots);
			++slot;
		}
		if (boots.getType() != Material.DIAMOND_BOOTS) {
			bootsInv.setItem(slot, GUIItems.diamond_boots);
			++slot;
		}
		if (boots.getType() != Material.THIN_GLASS) {
			bootsInv.setItem(slot, GUIItems.none);
		}
	}

	public void updateBootsEnchantments() {
		bootsInv.setItem(25, ItemUtil.setDisplayName(GUIItems.armour_protection.clone(), GUIItems.armour_protection.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("boots", Enchantment.PROTECTION_ENVIRONMENTAL) + "")));
		bootsInv.setItem(26, ItemUtil.setDisplayName(GUIItems.armour_thorns.clone(), GUIItems.armour_thorns.getItemMeta().getDisplayName().replace("{lvl}", getEnchantment("boots", Enchantment.THORNS) + "")));
	}

	private void updateEffectsInventory() {
		potionsInv.setItem(0, GUIItems.back);
		potionsInv.setItem(3, ItemUtil.setDisplayName(GUIItems.speed.clone(), GUIItems.speed.getItemMeta().getDisplayName().replace("{lvl}", speedLvL + "")));
		potionsInv.setItem(4, ItemUtil.setDisplayName(GUIItems.strength.clone(), GUIItems.strength.getItemMeta().getDisplayName().replace("{lvl}", strengthLvL + "")));
	}

	/* 1v1 equipment */
	public void giveItems() {
		PlayerManager.clearPlayer(p);
		PlayerInventory pi = p.getInventory();

		pi.setItem(1, GUIItems.edit_options);
	}

	public void equip(Player p) {
		PlayerManager.clearPlayer(p);

		PlayerInventory pi = p.getInventory();

		ItemStack helmet = this.helmet.clone();
		ItemStack chestplate = this.chestplate.clone();
		ItemStack leggings = this.leggings.clone();
		ItemStack boots = this.boots.clone();
		ItemStack sword = this.sword.clone();

		addEnchantments("helmet", helmet);
		addEnchantments("chestplate", chestplate);
		addEnchantments("leggings", leggings);
		addEnchantments("boots", boots);
		addEnchantments("sword", sword);

		if (!isNone(helmet)) {
			pi.setHelmet(helmet);
		}

		if (!isNone(chestplate)) {
			pi.setChestplate(chestplate);
		}

		if (!isNone(leggings)) {
			pi.setLeggings(leggings);
		}

		if (!isNone(boots)) {
			pi.setBoots(boots);
		}

		pi.setItem(0, sword);

	}

	private boolean isNone(ItemStack item) {
		return item.getType() == Material.THIN_GLASS;
	}

	private void addEnchantments(String forItem, ItemStack toEnchant) {
		if (!isNone(toEnchant)) {
			forItem = forItem.toLowerCase();
			if (enchantments.containsKey(forItem)) {
				for (Entry<Enchantment, Integer> entry : enchantments.get(forItem).entrySet()) {
					toEnchant.addUnsafeEnchantment(entry.getKey(), entry.getValue());
				}
			}
		}
	}

}
