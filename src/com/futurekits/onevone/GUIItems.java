package com.futurekits.onevone;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.futurekits.utilities.ItemUtil;

@SuppressWarnings("deprecation")
public class GUIItems {

	public static final ItemStack back = ItemUtil.setItemMeta(new ItemStack(Material.REDSTONE), ChatColor.DARK_RED + "<-- Back", ChatColor.GRAY + "Click to go to the previous page.");

	public static final ItemStack wood_sword = ItemUtil.setItemMeta(new ItemStack(Material.WOOD_SWORD), ChatColor.AQUA + "Wood", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Wood Sword");
	public static final ItemStack stone_sword = ItemUtil.setItemMeta(new ItemStack(Material.STONE_SWORD), ChatColor.AQUA + "Stone", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Stone Sword");
	public static final ItemStack gold_sword = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_SWORD), ChatColor.AQUA + "Gold", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Gold Sword");
	public static final ItemStack iron_sword = ItemUtil.setItemMeta(new ItemStack(Material.IRON_SWORD), ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Iron Sword");
	public static final ItemStack diamond_sword = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND_SWORD), ChatColor.AQUA + "Diamond", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Diamond Sword");

	public static final ItemStack none = ItemUtil.setItemMeta(new ItemStack(Material.THIN_GLASS), ChatColor.AQUA + "None", ChatColor.GRAY + "Click to select " + ChatColor.AQUA + "None");

	public static final ItemStack leather_helmet = ItemUtil.setItemMeta(new ItemStack(Material.LEATHER_HELMET), ChatColor.AQUA + "Leather", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Leather Helmet");
	public static final ItemStack chain_helmet = ItemUtil.setItemMeta(new ItemStack(Material.CHAINMAIL_HELMET), ChatColor.AQUA + "Chainmail", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Chainmail Helmet");
	public static final ItemStack gold_helmet = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_HELMET), ChatColor.AQUA + "Gold", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Gold Helmet");
	public static final ItemStack iron_helmet = ItemUtil.setItemMeta(new ItemStack(Material.IRON_HELMET), ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Iron Helmet");
	public static final ItemStack diamond_helmet = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND_HELMET), ChatColor.AQUA + "Diamond", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Diamond Helmet");

	public static final ItemStack leather_chestplate = ItemUtil.setItemMeta(new ItemStack(Material.LEATHER_CHESTPLATE), ChatColor.AQUA + "Chestplate", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Leather Chestplate");
	public static final ItemStack chain_chestplate = ItemUtil.setItemMeta(new ItemStack(Material.CHAINMAIL_CHESTPLATE), ChatColor.AQUA + "Chainmail", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Chainmail Chestplate");
	public static final ItemStack gold_chestplate = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_CHESTPLATE), ChatColor.AQUA + "Gold", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Gold Chestplate");
	public static final ItemStack iron_chestplate = ItemUtil.setItemMeta(new ItemStack(Material.IRON_CHESTPLATE), ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Iron Chestplate");
	public static final ItemStack diamond_chestplate = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND_CHESTPLATE), ChatColor.AQUA + "Diamond", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Diamond Chestplate");

	public static final ItemStack leather_leggings = ItemUtil.setItemMeta(new ItemStack(Material.LEATHER_LEGGINGS), ChatColor.AQUA + "Leather", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Leather Leggings");
	public static final ItemStack chain_leggings = ItemUtil.setItemMeta(new ItemStack(Material.CHAINMAIL_LEGGINGS), ChatColor.AQUA + "Chainmail", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Chainmail Leggings");
	public static final ItemStack gold_leggings = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_LEGGINGS), ChatColor.AQUA + "Gold", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Gold Leggings");
	public static final ItemStack iron_leggings = ItemUtil.setItemMeta(new ItemStack(Material.IRON_LEGGINGS), ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Iron Leggings");
	public static final ItemStack diamond_leggings = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND_LEGGINGS), ChatColor.AQUA + "Diamond", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Diamond Leggings");

	public static final ItemStack leather_boots = ItemUtil.setItemMeta(new ItemStack(Material.LEATHER_BOOTS), ChatColor.AQUA + "Leather", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Leather Boots");
	public static final ItemStack chain_boots = ItemUtil.setItemMeta(new ItemStack(Material.CHAINMAIL_BOOTS), ChatColor.AQUA + "Chainmail", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Chainmail Boots");
	public static final ItemStack gold_boots = ItemUtil.setItemMeta(new ItemStack(Material.GOLD_BOOTS), ChatColor.AQUA + "Gold", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Gold Boots");
	public static final ItemStack iron_boots = ItemUtil.setItemMeta(new ItemStack(Material.IRON_BOOTS), ChatColor.AQUA + "Iron", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Iron Boots");
	public static final ItemStack diamond_boots = ItemUtil.setItemMeta(new ItemStack(Material.DIAMOND_BOOTS), ChatColor.AQUA + "Diamond", ChatColor.GRAY + "Click to select this " + ChatColor.AQUA + " Diamond Boots");

	public static final ItemStack potion_effects = ItemUtil.setItemMeta(new ItemStack(373), ChatColor.GREEN + "Potion effects", ChatColor.GRAY + "Click to edit the potion effects.");

	public static final ItemStack speed = ItemUtil.setItemMeta(new ItemStack(Material.CHAINMAIL_BOOTS), ChatColor.AQUA + "Speed level: " + ChatColor.RED + "{lvl}", ChatColor.GRAY + "Click to toggle the speed level", ChatColor.GRAY + "The max speed level is 3.");
	public static final ItemStack strength = ItemUtil.setItemMeta(new ItemStack(Material.IRON_SWORD), ChatColor.AQUA + "Strength level: " + ChatColor.RED + "{lvl}", ChatColor.GRAY + "Click to toggle the strength level", ChatColor.GRAY + "The max strength level is 3.");

	public static final ItemStack sword_sharpness = ItemUtil.setItemMeta(new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 14), ChatColor.AQUA + "Sharpness level:" + ChatColor.RED + " {lvl}", ChatColor.GRAY + "Click to toggle the sharpness level", ChatColor.GRAY + "The max sharpness level is 3");
	public static final ItemStack sword_knockback = ItemUtil.setItemMeta(new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 5), ChatColor.AQUA + "Knockback level:" + ChatColor.RED + " {lvl}", ChatColor.GRAY + "Click to toggle the knockback level", ChatColor.GRAY + "The max knockback level is 3");
	public static final ItemStack sword_fireaspect = ItemUtil.setItemMeta(new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 11), ChatColor.AQUA + "Fire Aspect level:" + ChatColor.RED + " {lvl}", ChatColor.GRAY + "Click to toggle the fire aspect level", ChatColor.GRAY + "The max fire aspect level is 3");

	public static final ItemStack armour_protection = ItemUtil.setItemMeta(new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 14), ChatColor.AQUA + "Protection level:" + ChatColor.RED + " {lvl}", ChatColor.GRAY + "Click to toggle the protection level", ChatColor.GRAY + "The max protection level is 3");
	public static final ItemStack armour_thorns = ItemUtil.setItemMeta(new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 5), ChatColor.AQUA + "Thorns level:" + ChatColor.RED + " {lvl}", ChatColor.GRAY + "Click to toggle the thorns level", ChatColor.GRAY + "The max thorns level is 3");

	public static final ItemStack edit_options = ItemUtil.setItemMeta(new ItemStack(Material.EMERALD), ChatColor.GREEN + "Click to edit 1v1 options", ChatColor.GRAY + "Click to open the 1v1 option", ChatColor.GRAY + "editor to choose your equipment.");

}
