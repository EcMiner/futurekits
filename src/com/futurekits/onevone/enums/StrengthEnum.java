package com.futurekits.onevone.enums;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import com.futurekits.utilities.ItemUtil;

@SuppressWarnings("deprecation")
public enum StrengthEnum {

	STRENGTH_OFF(ItemUtil.setItemMeta(new ItemStack(373, 1, (byte) 9), ChatColor.GOLD + "Strength - " + ChatColor.RED + "Off", ChatColor.DARK_GRAY + "Right click to toggle")), STRENGTH_1(ItemUtil.setItemMeta(new ItemStack(373, 1, (byte) 9), ChatColor.GOLD + "Strength - " + ChatColor.GREEN + "I", ChatColor.DARK_GRAY + "Right click to toggle")), STRENGTH_2(
			ItemUtil.setItemMeta(new ItemStack(373, 1, (byte) 9), ChatColor.GOLD + "Strength - " + ChatColor.GREEN + "II", ChatColor.DARK_GRAY + "Right click to toggle"));

	private ItemStack item;

	private StrengthEnum(ItemStack item) {
		this.item = item;
	}

	public ItemStack getItem() {
		return item;
	}

	public static StrengthEnum toggle(StrengthEnum strength) {
		switch (strength) {
		case STRENGTH_OFF:
			return STRENGTH_1;
		case STRENGTH_1:
			return STRENGTH_2;
		case STRENGTH_2:
			return STRENGTH_OFF;
		default:
			return STRENGTH_OFF;
		}
	}

}
