package com.futurekits.onevone.enums;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.futurekits.utilities.ItemUtil;

public enum SwordEnum {

	WOOD(ItemUtil.setItemMeta(new ItemStack(Material.WOOD_SWORD), ChatColor.GOLD + "Sword - " + ChatColor.RED + "Wood", ChatColor.DARK_GRAY + "Right click to toggle.")), STONE(ItemUtil.setItemMeta(new ItemStack(Material.STONE_SWORD), ChatColor.GOLD + "Sword - " + ChatColor.YELLOW + "Stone", ChatColor.DARK_GRAY + "Right click to toggle.")), IRON(
			ItemUtil.setItemMeta(new ItemStack(Material.IRON_SWORD), ChatColor.GOLD + "Sword - " + ChatColor.GREEN + "Iron", ChatColor.DARK_GRAY + "Right click to toggle.")), DIAMOND(ItemUtil.setItemMeta(new ItemStack(Material.WOOD_SWORD), ChatColor.GOLD + "Sword - " + ChatColor.DARK_GREEN + "Diamond", ChatColor.DARK_GRAY
			+ "Right click to toggle."));

	private ItemStack item;

	private SwordEnum(ItemStack item) {
		this.item = item;
	}

	public ItemStack getItem() {
		return item;
	}

	public static SwordEnum toggle(SwordEnum sword) {
		switch (sword) {
		case WOOD:
			return STONE;
		case STONE:
			return IRON;
		case IRON:
			return DIAMOND;
		case DIAMOND:
			return WOOD;
		default:
			return IRON;
		}
	}

}
