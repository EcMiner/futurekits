package com.futurekits.onevone.enums;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import com.futurekits.utilities.ItemUtil;

@SuppressWarnings("deprecation")
public enum SpeedEnum {

	SPEED_OFF(ItemUtil.setItemMeta(new ItemStack(373, 1, (byte) 2), ChatColor.GOLD + "Speed - " + ChatColor.RED + "Off", ChatColor.DARK_GRAY + "Right click to toggle.")), SPEED_1(ItemUtil.setItemMeta(new ItemStack(373, 1, (byte) 2), ChatColor.GOLD + "Speed - " + ChatColor.GREEN + "I", ChatColor.DARK_GRAY + "Right click to toggle.")), SPEED_2(
			ItemUtil.setItemMeta(new ItemStack(373, 1, (byte) 2), ChatColor.GOLD + "Speed - " + ChatColor.GREEN + "II", ChatColor.DARK_GRAY + "Right click to toggle."));

	private ItemStack item;

	private SpeedEnum(ItemStack item) {
		this.item = item;
	}

	public ItemStack getItem() {
		return item;
	}

	public static SpeedEnum toggle(SpeedEnum speed) {
		switch (speed) {
		case SPEED_OFF:
			return SPEED_1;
		case SPEED_1:
			return SPEED_2;
		case SPEED_2:
			return SPEED_OFF;
		default:
			return SPEED_OFF;
		}
	}
}
