package com.futurekits.onevone.enums;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.futurekits.utilities.ItemUtil;

public enum RefillEnum {

	REFILL_OFF(ItemUtil.setItemMeta(new ItemStack(Material.BOWL), ChatColor.GOLD + "Refills - " + ChatColor.RED + "Off", ChatColor.DARK_GRAY + "Right click to toggle.")), REFILL_ON(ItemUtil.setItemMeta(new ItemStack(Material.MUSHROOM_SOUP), ChatColor.GOLD + "Refills - " + ChatColor.GREEN + "ON", ChatColor.DARK_GRAY + "Right click to toggle."));

	private ItemStack item;

	private RefillEnum(ItemStack item) {
		this.item = item;
	}

	public ItemStack getItem() {
		return item;
	}

	public static RefillEnum toggle(RefillEnum refill) {
		switch (refill) {
		case REFILL_OFF:
			return REFILL_ON;
		case REFILL_ON:
			return REFILL_OFF;
		default:
			return REFILL_OFF;
		}
	}

}
