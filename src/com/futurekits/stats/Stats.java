package com.futurekits.stats;

import java.util.UUID;

import org.bukkit.entity.Player;

public class Stats {

	private UUID playerId;

	private int kills = 0;
	private int deaths = 0;
	private int coins = 0;
	private int bestKillStreak = 0;
	private int currentKillStreak = 0;
	private String teamName;

	public Stats(Player p) {
		this(p.getUniqueId());
	}

	public Stats(UUID playerId) {
		this.playerId = playerId;

		Object[] stats = StatsManager.getStatistics(playerId);
		kills = (int) stats[0];
		deaths = (int) stats[1];
		coins = (int) stats[2];
		bestKillStreak = (int) stats[3];
		teamName = (String) stats[4];

		StatsManager.handleStatsInstance(this);
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public double getKDRatio() {
		return kills != 0 && deaths != 0 ? Double.valueOf(kills) / Double.valueOf(deaths) : (kills != 0 && deaths == 0 ? kills : 0.0);
	}

	public int getCoins() {
		return coins;
	}

	public int getBestKillStreak() {
		return bestKillStreak;
	}

	public int getCurrentKillStreak() {
		return currentKillStreak;
	}

	public String getTeamName() {
		return teamName;
	}

	public boolean isInTeam() {
		return teamName.replace(" ", "").length() > 0;
	}

	public void addKill() {
		addKills(1);
	}

	public void addDeath() {
		addDeaths(1);
	}

	public void addKills(int kills) {
		setKills(this.kills + kills);
	}

	public void addDeaths(int deaths) {
		setDeaths(this.deaths + deaths);
	}

	public void addCoins(int coins) {
		setCoins(this.coins + coins);
	}

	public void setKills(int kills) {
		addKillsToKillStreak(kills);
		this.kills = kills;
		StatsManager.setKills(playerId, kills);
	}

	public void addKillsToKillStreak(int kills) {
		this.currentKillStreak += kills - this.kills;
		if (this.currentKillStreak > this.bestKillStreak) {
			setBestKillStreak(this.currentKillStreak);
		}
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
		StatsManager.setDeaths(playerId, deaths);
	}

	public void setCoins(int coins) {
		this.coins = coins;
		StatsManager.setCoins(playerId, coins);
	}

	public void setBestKillStreak(int bestKillStreak) {
		this.bestKillStreak = bestKillStreak;
		StatsManager.setBestKillStreak(playerId, bestKillStreak);
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
		StatsManager.setTeam(playerId, teamName);
	}

	public UUID getPlayerId() {
		return playerId;
	}
}
