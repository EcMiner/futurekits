package com.futurekits.stats;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.futurekits.plugin.FutureKits;
import com.futurekits.sqlite.SQLite;

public class StatsManager {

	/* Stats storage */
	private static HashMap<UUID, Stats> allStats = new HashMap<UUID, Stats>();

	public static Stats getStats(Player p) {
		return getStats(p.getUniqueId());
	}

	public static Stats getStats(UUID playerId) {
		if (allStats.containsKey(playerId)) {
			return allStats.get(playerId);
		} else {
			return new Stats(playerId);
		}
	}

	public static void disposeStats(Player p) {
		disposeStats(p.getUniqueId());
	}

	public static void disposeStats(UUID playerId) {
		allStats.remove(playerId);
	}

	protected static void handleStatsInstance(Stats instance) {
		allStats.put(instance.getPlayerId(), instance);
	}

	private static SQLite db;

	public StatsManager(FutureKits plugin) {
		db = new SQLite(plugin, new File(plugin.getDataFolder(), "stats.sqlite"));
		initDB();
	}

	private void initDB() {
		db.updateQuery("create table if not exists stats(uuid varchar(255), kills int(11), deaths int(11), coins int(11), bestks int(11), team varchar(255))");
	}

	protected static Object[] getStatistics(Player p) {
		return getStatistics(p.getUniqueId());
	}

	protected static Object[] getStatistics(UUID playerId) {
		try {
			ResultSet set = db.executeQuery("select * from stats where uuid='" + playerId.toString() + "'");
			if (set.next()) {
				return new Object[] { set.getInt("kills"), set.getInt("deaths"), set.getInt("coins"), set.getInt("bestks"), set.getString("team") };
			} else {
				db.updateQuery("insert into stats(uuid, kills, deaths, coins, bestks, team) values('" + playerId.toString() + "', 0, 0, 0, 0, '')");
			}
			set.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new Object[] { 0, 0, 0, 0, "" };
	}

	/* Stats manipulation */
	protected static void setKills(UUID playerId, int kills) {
		setInt(playerId, "kills", kills);
	}

	protected static void setDeaths(UUID playerId, int deaths) {
		setInt(playerId, "deaths", deaths);
	}

	protected static void setCoins(UUID playerId, int coins) {
		setInt(playerId, "coins", coins);
	}

	protected static void setBestKillStreak(UUID playerId, int killStreak) {
		setInt(playerId, "bestks", killStreak);
	}

	protected static void setTeam(UUID playerId, String teamName) {
		setString(playerId, "team", teamName);
	}

	private static void setInt(UUID playerId, String setName, int value) {
		db.updateQuery("update stats set " + setName + "=" + value + " where uuid='" + playerId.toString() + "'");
	}

	private static void setString(UUID playerId, String setName, String value) {
		db.updateQuery("update stats set " + setName + "='" + value + "' where uuid='" + playerId.toString() + "'");
	}
}
