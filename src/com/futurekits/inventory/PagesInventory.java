package com.futurekits.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.futurekits.guis.kitgui.KitGUI;

public class PagesInventory {

	public static final Map<String, PagesInventory> allInventories = new HashMap<String, PagesInventory>();

	public static void callClickEvent(PagesInventory inventory, InventoryClickEvent e, int page) {
		inventory.handleClick(e, page);
	}

	private Map<Integer, Inventory> pages = new HashMap<Integer, Inventory>();
	private Map<String, Integer> activePage = new HashMap<String, Integer>();

	private String inventoryName;
	private int nextPageSlot = 8, previousPageSlot = 0, maxInventorySize = 6 * 9, minInventorySize = 9;
	private boolean staticSize;
	private ItemStack[] defaultContents;
	private List<Integer> clearSlots = new ArrayList<Integer>();
	private Map<String, Integer> viewingPage = new HashMap<String, Integer>();
	private ClickHandler clickHandler;

	private int index;

	public PagesInventory(String inventoryName, int nextPageSlot, int previousPageSlot, boolean staticSize, int maxInventorySize, int minInventorySize, ItemStack[] defaultContents) {
		this(inventoryName, nextPageSlot, previousPageSlot, staticSize, maxInventorySize, minInventorySize);

		for (int i = 0; i < defaultContents.length; i++) {
			if (this.defaultContents.length < i) {
				this.defaultContents[i] = defaultContents[i];
			} else {
				break;
			}
		}
	}

	public PagesInventory(String inventoryName, int nextPageSlot, int previousPageSlot, boolean staticSize, int maxInventorySize, int minInventorySize) {
		this.inventoryName = inventoryName;
		this.nextPageSlot = nextPageSlot;
		this.previousPageSlot = previousPageSlot;
		this.staticSize = staticSize;
		this.maxInventorySize = maxInventorySize;
		this.minInventorySize = minInventorySize < 9 ? 9 : minInventorySize;
		this.defaultContents = new ItemStack[maxInventorySize];

		addPage();

		allInventories.put(inventoryName, this);
	}

	public String getInventoryName() {
		return inventoryName;
	}

	public int getNextPageSlot() {
		return nextPageSlot;
	}

	public int getPreviousPageSlot() {
		return previousPageSlot;
	}

	public boolean getStaticSize() {
		return staticSize;
	}

	public int getMaxInventorySize() {
		return maxInventorySize;
	}

	public ItemStack[] getDefaultContents() {
		return defaultContents;
	}

	public void setDefaultItem(int slot, ItemStack item) {
		if (slot < defaultContents.length) {
			defaultContents[slot] = item;
		} else {
			throw new NullPointerException("The slot " + slot + " is exceeding the max inventory size of " + maxInventorySize);
		}
	}

	public void addClearSlots(Integer... slots) {
		clearSlots.addAll(Arrays.asList(slots));
	}

	public void addItem(ItemStack item) {
		int slot = getSlot();

		if (slot > maxInventorySize) {
			addPage();
			addItem(item);
			return;
		} else {
			pages.get(pages.size()).setItem(slot, item);
		}
	}

	public Inventory getPage(int page) {
		return pages.get(page < 1 ? 1 : (page > pages.size() ? pages.size() : page));
	}

	public void closePage(Player p) {
		activePage.remove(p.getName());
	}

	private void addPage() {
		index = 0;
		int page = pages.size() + 1;

		Inventory inv = Bukkit.createInventory(null, staticSize ? maxInventorySize : minInventorySize, inventoryName);
		pages.put(page, inv);

		addDefaultItems(page);

		if (page != 1) {
			addPreviousPageItem(page);
			addNextPageItem(page - 1);
		}
	}

	private int getSlot() {
		Inventory inv = pages.get(pages.size());

		if (index < inv.getSize() - 1 && index < maxInventorySize) {
			while ((inv.getItem(index) != null || index == nextPageSlot || index == previousPageSlot || clearSlots.contains(index)) && index <= maxInventorySize) {
				index++;
			}
		} else {
			if (index < maxInventorySize) {
				addRow(pages.size());
				return getSlot();
			} else {
				return inv.getSize();
			}
		}

		return index;
	}

	private void addDefaultItems(int page) {
		addDefaultItems(page, 0, pages.get(page).getSize() - 1);
	}

	private void addDefaultItems(int page, int startIndex, int endIndex) {
		Validate.notNull(pages.get(page), "The page " + page + " does not exist!");

		addDefaultItems(pages.get(page), startIndex, endIndex);
	}

	private void addDefaultItems(Inventory inv, int startIndex, int endIndex) {
		startIndex = startIndex < 0 ? 0 : startIndex;
		endIndex = endIndex >= inv.getSize() ? inv.getSize() - 1 : endIndex;

		for (int i = startIndex; i <= endIndex; i++) {
			if (defaultContents.length > i && defaultContents[i] != null) {
				inv.setItem(i, defaultContents[i]);
			}
		}
	}

	private void addNextPageItem(int page) {
		Validate.notNull(pages.get(page), "The page " + page + " does not exist!");

		Inventory inv = pages.get(page);

		while (nextPageSlot >= inv.getSize()) {
			addRow(page);
			inv = pages.get(page);
		}

		inv.setItem(nextPageSlot, KitGUI.nextPage);
	}

	private void addPreviousPageItem(int page) {
		Validate.notNull(pages.get(page), "The page " + page + " does not exist!");

		Inventory inv = pages.get(page);

		while (previousPageSlot >= inv.getSize()) {
			addRow(page);
			inv = pages.get(page);
		}

		inv.setItem(previousPageSlot, KitGUI.previousPage);
	}

	private void addRow(int page) {
		setInventorySize(page, pages.get(page).getSize() + 9);
	}

	private void setInventorySize(int page, int size) {
		Validate.notNull(pages.get(page), "The page " + page + " does not exist!");

		Inventory inv = Bukkit.createInventory(null, size, inventoryName);
		inv.setContents(pages.get(page).getContents());
		addDefaultItems(inv, pages.get(page).getSize(), size);

		pages.put(page, inv);
	}

	private void handleClick(InventoryClickEvent e, int page) {
		if (clickHandler != null) {
			int slot = e.getRawSlot();
			Player p = (Player) e.getWhoClicked();
			InventoryAction action = e.getAction();
			int hotBarButton = e.getHotbarButton();
			ItemStack itemClicked = e.getCurrentItem();
			clickHandler.onClick(page, slot, p, action, hotBarButton, itemClicked);
		}
	}

	public void addClickHandler(ClickHandler clickHandler) {
		this.clickHandler = clickHandler;
		clickHandler.inventory = this;
	}

	public int getViewingPage(Player p) {
		return viewingPage.containsKey(p.getName()) ? viewingPage.get(p.getName()) : 1;
	}

	public void setViewingPage(Player p, int page) {
		page = page < 1 ? 1 : (page > pages.size() ? pages.size() : page);
		viewingPage.put(p.getName(), page);
	}

	public void removeViewingPage(Player p) {
		viewingPage.remove(p.getName());
	}

	public static abstract class ClickHandler {

		public PagesInventory inventory;

		public abstract void onClick(int page, int slot, Player p, InventoryAction action, int hotBarButton, ItemStack itemClicked);

	}

}
