package com.futurekits.plugin;

import java.text.NumberFormat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.futurekits.eventsystem.EventManager;
import com.futurekits.guis.kitgui.KitGUI;
import com.futurekits.listeners.PagesInventoryListener;
import com.futurekits.listeners.PlayerListener;
import com.futurekits.listeners.StatsListener;
import com.futurekits.managers.KitManager;
import com.futurekits.managers.PlayerManager;
import com.futurekits.onevone.PlayerData;
import com.futurekits.onevone.listeners.OptionListener;
import com.futurekits.sqlite.SQLite;
import com.futurekits.stats.Stats;
import com.futurekits.stats.StatsManager;

public class FutureKits extends JavaPlugin {

	private static FutureKits instance;

	@SuppressWarnings("deprecation")
	public void onEnable() {
		instance = this;

		if (!getDataFolder().exists()) {
			getDataFolder().mkdirs();
		}

		new KitManager(this);
		new PlayerManager(this);
		new StatsManager(this);
		new KitGUI(this);
		new EventManager(this);

		registerListener();

		for (Player p : Bukkit.getOnlinePlayers()) {
			new Stats(p);
			new PlayerData(p).giveItems();
		}
	}

	public void onDisable() {
		SQLite.closeAll();
	}

	private void registerListener() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new StatsListener(), this);
		pm.registerEvents(new PlayerListener(), this);
		pm.registerEvents(new PagesInventoryListener(), this);
		pm.registerEvents(new OptionListener(), this);
	}

	public static FutureKits getInstance() {
		return instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("stats")) {
			PlayerManager.openStatsInventory((Player) sender);
		} else if (command.getName().equalsIgnoreCase("lag")) {
			Player p = (Player) sender;
			Runtime runtime = Runtime.getRuntime();
			NumberFormat format = NumberFormat.getInstance();
			StringBuilder sb = new StringBuilder();
			long maxMemory = (runtime.maxMemory() / 1024) / 1000;
			long allocatedMemory = (runtime.totalMemory() / 1024) / 1000;
			long freeMemory = (runtime.freeMemory() / 1024) / 1000;

			sb.append("free memory: " + format.format(freeMemory) + "GB\n");
			sb.append("allocated memory: " + format.format(allocatedMemory) + "GB\n");
			sb.append("max memory: " + format.format(maxMemory) + "GB\n");
			sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory))) + "GB\n");
			p.sendMessage(sb.toString());
		} else if (command.getName().equalsIgnoreCase("givecoins")) {
			StatsManager.getStats((Player) sender).addCoins(Integer.parseInt(args[0]));
		} else if (command.getName().equalsIgnoreCase("1v1")) {
			Player p = (Player) sender;
			// new PlayerData(p).giveItems();
		}
		return false;
	}
}