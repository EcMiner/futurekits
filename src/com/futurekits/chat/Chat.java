package com.futurekits.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Chat {

	public static final String prefix = ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "> ";

	public static final ChatColor defaultColor = ChatColor.GOLD;
	public static final ChatColor markingColor = ChatColor.GREEN;

	public static final ChatColor defaultErrorColor = ChatColor.RED;
	public static final ChatColor errorMarkingColor = ChatColor.DARK_RED;

	public static final ChatColor secondMarkingColor = ChatColor.WHITE;

	public static void sendMessage(Player p, String message) {
		p.sendMessage(prefix + defaultColor + message);
	}

	public static void sendErrorMessage(Player p, String message) {
		p.sendMessage(prefix + defaultErrorColor + message);
	}

}
