package com.futurekits.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import com.futurekits.inventory.PagesInventory;
import com.futurekits.utilities.InventoryUtil;

public class PagesInventoryListener implements Listener {

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (PagesInventory.allInventories.containsKey(e.getInventory().getTitle()) && InventoryUtil.isValid(e.getInventory(), e.getRawSlot())) {
			e.setCancelled(true);

			PagesInventory pi = PagesInventory.allInventories.get(e.getInventory().getTitle());
			Player p = (Player) e.getWhoClicked();

			if (e.getRawSlot() == pi.getNextPageSlot()) {
				int nextPage = pi.getViewingPage(p) + 1;
				p.openInventory(pi.getPage(nextPage));
				pi.setViewingPage(p, nextPage);
			} else if (e.getRawSlot() == pi.getPreviousPageSlot()) {
				int previousPage = pi.getViewingPage(p) - 1;
				p.openInventory(pi.getPage(previousPage));
				pi.setViewingPage(p, previousPage);
			} else {
				int page = pi.getViewingPage(p);
				PagesInventory.callClickEvent(pi, e, page);
			}
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (PagesInventory.allInventories.containsKey(e.getInventory().getTitle())) {
			PagesInventory pi = PagesInventory.allInventories.get(e.getInventory().getTitle());
			Player p = (Player) e.getPlayer();
			pi.removeViewingPage(p);
		}
	}

}
