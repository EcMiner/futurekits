package com.futurekits.listeners;

import java.lang.reflect.Field;
import java.util.Iterator;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftHumanEntity;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.PermissionAttachment;

import com.futurekits.eventsystem.EventManager;
import com.futurekits.guis.kitgui.KitGUI;
import com.futurekits.managers.PlayerManager;
import com.futurekits.onevone.PlayerData;
import com.futurekits.permissions.CustomPermissable;
import com.futurekits.plugin.FutureKits;
import com.futurekits.utilities.ItemUtil;

public class PlayerListener implements Listener {

	@EventHandler
	public void playerJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		PlayerInventory inv = p.getInventory();

		inv.clear();
		inv.setArmorContents(null);

		PlayerManager.clearPlayer(p);
		PlayerManager.equipSpawnItems(p);

		new PlayerData(p).giveItems();

		injectPlayer(p);

		PermissionAttachment pa = p.addAttachment(FutureKits.getInstance());

		pa.setPermission("futurekits.kit.pvp", true);

		long start = System.currentTimeMillis();

		p.hasPermission("futurekits.kit.archer");

		System.out.println("Took: " + (System.currentTimeMillis() - start));

		// TODO Teleport to spawn
	}

	private void injectPlayer(final Player p) {
		try {
			CustomPermissable pi = new CustomPermissable(p);

			CraftHumanEntity cp = ((CraftHumanEntity) p);

			Class<?> c = cp.getClass().getSuperclass();

			Field f = c.getDeclaredField("perm");

			f.setAccessible(true);

			f.set(cp, pi);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@EventHandler
	public void foodLevelChange(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void playerRespawn(PlayerRespawnEvent e) {
		Player p = e.getPlayer();
		PlayerManager.equipSpawnItems(p);
	}

	@EventHandler
	public void onSoup(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player p = e.getPlayer();
			if (p.getItemInHand() != null && p.getItemInHand().getType() == Material.MUSHROOM_SOUP) {
				Damageable d = p;
				if (Math.round(d.getHealth()) < 20) {
					d.setHealth(d.getHealth() + 7 >= d.getMaxHealth() ? d.getMaxHealth() : Math.round(d.getHealth() + 7d));
					p.getItemInHand().setType(Material.BOWL);
					p.getItemInHand().setItemMeta(null);
				}
			}
		}
	}

	/* Default spawn items e.g. Kit Selector etc. listeners */

	@EventHandler
	public void interactWithSpawnItems(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() != Action.PHYSICAL && p.getItemInHand() != null) {
			if (ItemUtil.isSameItem(p.getItemInHand(), PlayerManager.kitSelector)) {
				e.setCancelled(true);
				p.openInventory(KitGUI.getPage(p, 1));
			} else if (ItemUtil.isSameItem(p.getItemInHand(), PlayerManager.yourStats)) {
				e.setCancelled(true);
				PlayerManager.openStatsInventory(p);
			} else if (ItemUtil.isSameItem(p.getItemInHand(), PlayerManager.eventMenu)) {
				p.openInventory(EventManager.inventory.getPage(1));
				e.setCancelled(true);
			} else if (ItemUtil.isSameItem(p.getItemInHand(), PlayerManager.teleport1v1)) {
				e.setCancelled(true);
				p.getItemInHand().setDurability((short) 0);
				p.updateInventory();
			} else if (ItemUtil.isSameItem(p.getItemInHand(), PlayerManager.kitShop)) {
				e.setCancelled(true);
				p.openInventory(KitGUI.getShopPage(p, 1));
			}
		}
	}

	@EventHandler
	public void dropSpawnItems(PlayerDropItemEvent e) {
		if (ItemUtil.isSameItem(e.getItemDrop().getItemStack(), PlayerManager.kitSelector)) {
			e.setCancelled(true);
		} else if (ItemUtil.isSameItem(e.getItemDrop().getItemStack(), PlayerManager.yourStats)) {
			e.setCancelled(true);
		} else if (ItemUtil.isSameItem(e.getItemDrop().getItemStack(), PlayerManager.eventMenu)) {
			e.setCancelled(true);
		} else if (ItemUtil.isSameItem(e.getItemDrop().getItemStack(), PlayerManager.teleport1v1)) {
			e.setCancelled(true);
		} else if (ItemUtil.isSameItem(e.getItemDrop().getItemStack(), PlayerManager.kitShop)) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void clickSpawnItems(InventoryClickEvent e) {
		if (e.getInventory().getType() == InventoryType.CRAFTING && e.getCurrentItem() != null) {
			if (ItemUtil.isSameItem(e.getCurrentItem(), PlayerManager.kitSelector)) {
				e.setCancelled(true);
			} else if (ItemUtil.isSameItem(e.getCurrentItem(), PlayerManager.yourStats)) {
				e.setCancelled(true);
			} else if (ItemUtil.isSameItem(e.getCurrentItem(), PlayerManager.eventMenu)) {
				e.setCancelled(true);
			} else if (ItemUtil.isSameItem(e.getCurrentItem(), PlayerManager.teleport1v1)) {
				e.setCancelled(true);
			} else if (ItemUtil.isSameItem(e.getCurrentItem(), PlayerManager.kitShop)) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void playerDeath(PlayerDeathEvent e) {
		Iterator<ItemStack> iterator = e.getDrops().iterator();
		while (iterator.hasNext()) {
			ItemStack is = iterator.next();
			if (ItemUtil.isSameItem(is, PlayerManager.kitSelector)) {
				iterator.remove();
			} else if (ItemUtil.isSameItem(is, PlayerManager.yourStats)) {
				iterator.remove();
			} else if (ItemUtil.isSameItem(is, PlayerManager.eventMenu)) {
				iterator.remove();
			} else if (ItemUtil.isSameItem(is, PlayerManager.teleport1v1)) {
				iterator.remove();
			} else if (ItemUtil.isSameItem(is, PlayerManager.kitShop)) {
				iterator.remove();
			}
		}
	}

}
