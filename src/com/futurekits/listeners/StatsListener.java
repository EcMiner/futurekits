package com.futurekits.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.futurekits.stats.Stats;
import com.futurekits.stats.StatsManager;
import com.futurekits.utilities.InventoryUtil;

public class StatsListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void playerJoin(PlayerJoinEvent e) {
		new Stats(e.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void playerQuit(PlayerQuitEvent e) {
		StatsManager.disposeStats(e.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void playerDeathAndKill(PlayerDeathEvent e) {
		Player p = e.getEntity();
		StatsManager.getStats(p).addDeath();
		if (p.getKiller() != null) {
			StatsManager.getStats(p.getKiller()).addKill();
		}
	}

	@EventHandler
	public void statsInvClick(InventoryClickEvent e) {
		if (e.getInventory().getTitle().equals(ChatColor.GREEN + "Your Stats") && InventoryUtil.isValid(e.getInventory(), e.getRawSlot())) {
			e.setCancelled(true);
		}
	}

}
