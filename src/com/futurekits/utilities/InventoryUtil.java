package com.futurekits.utilities;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryUtil {

	public static boolean isValid(Inventory inv, int slot) {
		ItemStack is = null;
		try {
			is = inv.getItem(slot);
		} catch (Exception e) {
			return false;
		}
		return is != null;
	}

}
