package com.futurekits.permissions;

import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.ServerOperator;

public class CustomPermissable extends PermissibleBase {

	public CustomPermissable(ServerOperator opable) {
		super(opable);
	}

	@Override
	public boolean hasPermission(Permission perm) {
		if (!super.hasPermission(perm)) {
			return hasPermission(perm.getName());
		} else {
			return true;
		}
	}

	@Override
	public boolean hasPermission(String inName) {
		if (!super.hasPermission(inName)) {
			if (super.hasPermission("*")) {
				return true;
			} else {
				if (inName.contains(".")) {
					String[] s = inName.toLowerCase().split("\\.");
					if (s.length > 1) {
						String perm = "";
						for (int i = 0; i < s.length - 1; i++) {
							perm += (perm == "" ? "" : ".") + s[i];
							if (super.hasPermission(perm + ".*")) {
								return true;
							}
						}
					}
				}
				return false;
			}
		} else {
			return true;
		}
	}
}
