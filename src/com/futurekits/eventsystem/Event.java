package com.futurekits.eventsystem;

import java.io.File;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.futurekits.configuration.Config;
import com.futurekits.plugin.FutureKits;
import com.futurekits.utilities.ItemUtil;

public abstract class Event implements Listener {

	private String eventName, description;
	private ItemStack icon;
	private List<String> formattedDescription;
	private int maxTime, minPlayers, maxPlayers, playersJoined;
	private boolean playerAmountMustBeEven;
	private Config config;

	public Event(String eventName, ItemStack icon, String description, int maxTime, int minPlayers, int maxPlayers, boolean playerAmountMustBeEvent) {
		this.eventName = eventName;
		this.description = description;
		this.formattedDescription = ItemUtil.formatLore(ChatColor.BLUE, description);
		this.icon = ItemUtil.setItemMeta(icon, ChatColor.GREEN + eventName + " Event", formattedDescription);
		this.maxTime = maxTime;
		this.minPlayers = minPlayers;
		this.maxPlayers = maxPlayers;
		this.playerAmountMustBeEven = playerAmountMustBeEvent;
		this.config = new Config(new File(FutureKits.getInstance().getDataFolder() + "/events/" + eventName + ".yml"));
	}

	public final String getEventName() {
		return eventName;
	}

	public final ItemStack getIcon() {
		return icon;
	}

	public final String getDescription() {
		return description;
	}

	public final List<String> getFormattedDescription() {
		return formattedDescription;
	}

	public final int getMaxTime() {
		return maxTime;
	}

	public final int getMinPlayers() {
		return minPlayers;
	}

	public final int getMaxPlayers() {
		return maxPlayers;
	}

	public final boolean getPlayerAmountMustBeEven() {
		return playerAmountMustBeEven;
	}

	public final int getPlayersJoined() {
		return playersJoined;
	}

	public final FileConfiguration getConfig() {
		return config.getConfig();
	}

	public final void saveConfig() {
		config.saveConfig();
	}

	public abstract boolean isCorrectlySetup();

	public abstract void onEventStart();

	public abstract void onEventForceStop();

	public abstract void start();

	public abstract void stop();

	public abstract void onTick();

}
