package com.futurekits.eventsystem;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;

import com.futurekits.inventory.PagesInventory;
import com.futurekits.inventory.PagesInventory.ClickHandler;
import com.futurekits.plugin.FutureKits;

public class EventManager {

	private static Map<String, Event> events = new HashMap<String, Event>();

	private FutureKits plugin;

	public static PagesInventory inventory;

	public EventManager(FutureKits plugin) {
		this.plugin = plugin;

		inventory = new PagesInventory(ChatColor.GREEN + "Event Menu", 53, 45, false, 5 * 9, 9);

		inventory.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(int page, int slot, Player p, InventoryAction action, int hotBarButton, ItemStack itemClicked) {
				System.out.println(itemClicked.getItemMeta().getDisplayName());
			}

		});

		loadEvents();

	}

	private void loadEvents() {

	}

	private void handleEventInstance(Event event) {
		events.put(event.getEventName(), event);
		inventory.addItem(event.getIcon());
	}

}
