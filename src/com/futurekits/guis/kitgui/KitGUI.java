package com.futurekits.guis.kitgui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.futurekits.kit.AbstractKit;
import com.futurekits.managers.KitManager;
import com.futurekits.plugin.FutureKits;
import com.futurekits.utilities.ItemUtil;

public class KitGUI {

	/* Items */
	public static final ItemStack nextPage = ItemUtil.setItemMeta(new ItemStack(Material.CARPET), ChatColor.GREEN + "Next Page -->");
	public static final ItemStack previousPage = ItemUtil.setItemMeta(new ItemStack(Material.CARPET), ChatColor.GREEN + "<-- Previous Page");
	public static final ItemStack nothing = ItemUtil.setItemMeta(new ItemStack(Material.THIN_GLASS), ChatColor.GRAY.toString());

	/* Page storage */
	private static final Map<Integer, Inventory> pages = new HashMap<Integer, Inventory>();
	private static final Map<Integer, Map<Integer, AbstractKit>> kitInSlot = new HashMap<Integer, Map<Integer, AbstractKit>>();

	public KitGUI(FutureKits plugin) {
		plugin.getServer().getPluginManager().registerEvents(new KitGUIListener(), plugin);
		addPage();
		for (AbstractKit kit : KitManager.getKits()) {
			addKitToInventory(kit);
		}
	}

	private void addKitToInventory(AbstractKit kit) {
		Inventory inv = getCurrentInventory();
		int page = pages.size();
		int kits = kitInSlot.get(page).size();
		if (kits <= inv.getSize() - 2) {
			List<String> lore = new ArrayList<String>();
			lore.addAll(kit.getFormattedDescription());
			lore.add("");
			lore.add(ChatColor.GREEN + "You own this kit.");
			inv.setItem(9 + kits, ItemUtil.setItemMeta(kit.getIcon().clone(), ChatColor.GREEN + kit.getName(), lore));
			kitInSlot.get(page).put(9 + kits, kit);
		} else {
			addPage();
			addKitToInventory(kit);
		}
	}

	private Inventory getCurrentInventory() {
		Inventory inv = null;
		if (!pages.containsKey(1)) {
			addPage();
			return getCurrentInventory();
		} else {
			inv = pages.get(pages.size());
		}
		return inv;
	}

	private void addPage() {
		Inventory inv = Bukkit.createInventory(null, 9 * 6, ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Kit Selector");
		for (int i = 0; i < 9; i++) {
			inv.setItem(i, nothing);
		}

		int page = pages.size() + 1;

		pages.put(page, inv);
		kitInSlot.put(page, new HashMap<Integer, AbstractKit>());

		if (page != 1) {
			inv.setItem(0, previousPage);
			pages.get(page - 1).setItem(8, nextPage);
		}
	}

	public static Inventory getPage(Player p, int page) {
		page = (page < pages.size() ? 1 : (page > pages.size() ? 1 : page));
		Inventory inv = Bukkit.createInventory(null, 9 * 6, ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Kit Selector");
		inv.setContents(pages.get(page).getContents());
		for (Entry<Integer, AbstractKit> entry : kitInSlot.get(page).entrySet()) {
			if (!p.hasPermission(entry.getValue().getPermission())) {
				List<String> lore = inv.getItem(entry.getKey()).getItemMeta().getLore();
				lore.remove(lore.size() - 1);
				lore.add(ChatColor.RED + "You do not own this kit.");
				ItemUtil.setItemMeta(inv.getItem(entry.getKey()), ChatColor.RED + entry.getValue().getName(), lore);
			}
		}
		return inv;
	}

	public static Inventory getShopPage(Player p, int page) {
		page = (page < pages.size() ? 1 : (page > pages.size() ? 1 : page));
		Inventory inv = Bukkit.createInventory(null, 9 * 6, ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Shop Kits");
		inv.setContents(pages.get(page).getContents());
		for (Entry<Integer, AbstractKit> entry : kitInSlot.get(page).entrySet()) {
			if (!p.hasPermission(entry.getValue().getPermission())) {
				List<String> lore = inv.getItem(entry.getKey()).getItemMeta().getLore();
				lore.remove(lore.size() - 1);
				lore.add(ChatColor.RED + "You do not own this kit.");
				lore.add("");
				lore.add(ChatColor.GOLD + "Price" + ChatColor.WHITE + ": " + ChatColor.GREEN + entry.getValue().getPrice());
				ItemUtil.setItemMeta(inv.getItem(entry.getKey()), ChatColor.RED + entry.getValue().getName(), lore);
			}
		}
		return inv;
	}

	public static AbstractKit getKit(int page, int slot) {
		if (kitInSlot.containsKey(page) && kitInSlot.get(page).containsKey(slot)) {
			return kitInSlot.get(page).get(slot);
		}
		return null;
	}

	public static int getPages() {
		return pages.size();
	}

}
