package com.futurekits.guis.kitgui;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.futurekits.chat.Chat;
import com.futurekits.kit.AbstractKit;
import com.futurekits.stats.Stats;
import com.futurekits.stats.StatsManager;
import com.futurekits.utilities.InventoryUtil;

public class KitGUIListener implements Listener {

	private Map<String, Integer> pageSelected = new HashMap<String, Integer>();

	public KitGUIListener() {
	}

	@EventHandler
	public void clickGUI(InventoryClickEvent e) {
		if (e.getWhoClicked() instanceof Player && InventoryUtil.isValid(e.getInventory(), e.getRawSlot())) {
			if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Kit Selector")) {
				e.setCancelled(true);
				Player p = (Player) e.getWhoClicked();
				int page = getPage(p);
				if (e.getRawSlot() <= 8) {
					if (e.getCurrentItem() == KitGUI.nextPage) {
						p.openInventory(KitGUI.getPage(p, page + 1 <= KitGUI.getPages() ? page + 1 : KitGUI.getPages()));
						pageSelected.put(p.getName(), page + 1);
					} else if (e.getCurrentItem() == KitGUI.previousPage) {
						p.openInventory(KitGUI.getPage(p, page - 1));
						pageSelected.put(p.getName(), page - 1 >= 1 ? page - 1 : 1);
					}
				} else {
					AbstractKit kit = KitGUI.getKit(page, e.getRawSlot());
					if (kit != null) {
						if (p.hasPermission(kit.getPermission())) {
							p.closeInventory();
							kit.equip(p);
						}
					}
				}
			} else if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Shop Kits")) {
				e.setCancelled(true);
				Player p = (Player) e.getWhoClicked();
				int page = getPage(p);
				if (e.getRawSlot() <= 8) {
					if (e.getCurrentItem() == KitGUI.nextPage) {
						p.openInventory(KitGUI.getShopPage(p, page + 1 <= KitGUI.getPages() ? page + 1 : KitGUI.getPages()));
						pageSelected.put(p.getName(), page + 1);
					} else if (e.getCurrentItem() == KitGUI.previousPage) {
						p.openInventory(KitGUI.getShopPage(p, page - 1));
						pageSelected.put(p.getName(), page - 1 >= 1 ? page - 1 : 1);
					}
				} else {
					AbstractKit kit = KitGUI.getKit(page, e.getRawSlot());
					if (kit != null) {
						if (!p.hasPermission(kit.getPermission())) {
							Stats s = StatsManager.getStats(p);
							if (s.getCoins() - kit.getPrice() >= 0) {
								p.closeInventory();
								Chat.sendMessage(p, "You successfully bought the kit" + Chat.secondMarkingColor + ": " + Chat.markingColor + kit.getName());
								Chat.sendMessage(p, "Coins left" + Chat.secondMarkingColor + ": " + Chat.markingColor + (s.getCoins() - kit.getPrice()));
								// PermissionUser pu = PermissionsEx.getUser(p);
								// pu.addPermission(kit.getPermission().getName());
								s.addCoins(-kit.getPrice());
							} else {
								p.closeInventory();
								Chat.sendErrorMessage(p, "You don't have enough coins to buy this kit!");
							}
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void closeInventory(InventoryCloseEvent e) {
		if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Kit Selector")) {
			pageSelected.remove(e.getPlayer().getName());
		} else if (e.getInventory().getTitle().equalsIgnoreCase(ChatColor.DARK_PURPLE + "FutureKits " + ChatColor.DARK_GRAY + "-" + ChatColor.DARK_GREEN + " Shop Kits")) {
			pageSelected.remove(e.getPlayer().getName());
		}
	}

	@EventHandler
	public void playerQuit(PlayerQuitEvent e) {
		if (pageSelected.containsKey(e.getPlayer().getName())) {
			pageSelected.remove(e.getPlayer().getName());
		}
	}

	private int getPage(Player p) {
		return pageSelected.containsKey(p.getName()) ? pageSelected.get(p.getName()) : 1;
	}

}
